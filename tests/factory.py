import factory as factory_boy


class UserFactory(factory_boy.Factory):
    # class Meta:
    #     model = users.models.User

    f_name = factory_boy.Faker("first_name")
    l_name = factory_boy.Faker("last_name")
    nickname = factory_boy.Faker("first_name")
    email = factory_boy.LazyAttribute(
        lambda x: f"{x.f_name}.{x.l_name}@example.com".lower()
    )
    phone_number = factory_boy.Faker("phone_number", locale="en-GB")
    password_hash = (  # apollo-test
        "bcrypt_sha256$$2b$12$PHXZlF2RUzY9v7imeyvxP.scRXIHm5RmZz1Jl5GoZ.1usbKI20EL2"
    )
    otp_secret = "MQZM2H3DCEBWUMRXVZ3IDIHMEF5V4U7W"
    fcm_token = factory_boy.Faker("word")
    emergency_phone_number = factory_boy.Faker("phone_number", locale="en-GB")


class Factory:
    user = UserFactory
