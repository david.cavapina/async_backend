import pytest


@pytest.mark.asyncio
async def test_status_returns_200(client):
    response = await client.get("/status/ok")
    assert response.status_code == 200
