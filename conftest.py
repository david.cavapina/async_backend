# All methods/stubs/hooks you want available for your tests in the current directory
import asyncio

import databases
import httpx
import pytest
from alembic import command
from alembic.config import Config
from sqlalchemy import create_engine

from api import api, config, db, logging
from tests.factory import Factory

config.TESTING = True
log = logging.get_logger(__name__)


def run_migrations(dsn: str) -> None:
    script_location = str(Path(__file__).parent / "migrations")
    alembic_cfg = Config()
    alembic_cfg.set_main_option("script_location", script_location)
    alembic_cfg.set_main_option("sqlalchemy.url", dsn)
    command.upgrade(alembic_cfg, "head")


@pytest.fixture(scope="session")
async def db_engine():
    """
    Runs migrations and drops all tables once the session is finished
    Assums the test database is present so it must be created beforehand.
    """
    if not str(config.DATABASE_URL).endswith("_test"):
        config.DATABASE_URL = config.DATABASE_URL.replace(
            database=config.DATABASE_URL.database + "_test"
        )

    database_url = str(config.DATABASE_URL)
    run_migrations(database_url)
    utils.create_dynamo_tables()

    db.db = databases.Database(database_url, force_rollback=True)
    await db.db.connect()
    yield db.db
    try:
        await asyncio.wait_for(db.db.disconnect(), 1.0)
    except asyncio.TimeoutError:
        log.warning("Timeout disconnecting from the test DB")

    engine = create_engine(database_url)
    db.metadata.drop_all(engine)
    utils.drop_dynamo_tables()


@pytest.fixture()
async def database(db_engine):
    """Sets the API's Database instance pointing to the test DB."""
    async with db.db.transaction(force_rollback=True):
        yield db.db


@pytest.fixture
def factory(database):
    return Factory()


@pytest.yield_fixture(scope="session")
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def factory(database):
    return Factory()


@pytest.fixture
async def client():
    async with httpx.AsyncClient(app=api.app, base_url="http://testserver") as client:
        yield client
