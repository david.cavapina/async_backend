from databases import DatabaseURL
from starlette.config import Config
from starlette.datastructures import Secret


config = Config(".env")

ENVIRONMENT: str = config("ENVIRONMENT", cast=str, default="local")
VERSION: str = config("VERSION", cast=str, default="local")
DEBUG: bool = config("DEBUG", cast=bool, default=False)
TESTING: bool = config("TESTING", cast=bool, default=False)
LOG_LEVEL: str = config("LOG_LEVEL", cast=str, default="INFO")

# database
DB_NAME: str = config("DB_NAME", cast=str, default="belessons")
DB_PASS: Secret = config("DB_PASS", cast=Secret, default="")
DB_HOST: str = config("DB_HOST", cast=str, default="localhost")
DB_PORT: int = config("DB_PORT", cast=int, default=5432)
DB_USER: str = config("DB_USER", cast=str, default=None)
DATABASE_URL: DatabaseURL = DatabaseURL(
    "postgresql://{0}{1}:{2}/{3}".format(
        DB_USER + ":" + str(DB_PASS) + "@" if DB_USER else "",
        DB_HOST,
        DB_PORT,
        DB_NAME,
    )
)
