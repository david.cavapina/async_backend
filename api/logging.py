from uuid import uuid4
import sys
import logging
import logging.config
from time import perf_counter
from typing import Any
from starlette.types import ASGIApp, Receive, Scope, Send
import structlog
from api import config


def setup_logging(level: int = logging.INFO):
    if config.ENVIRONMENT != "local":
        logging.config.dictConfig(
            {
                "version": 1,
                "formatters": {
                    "json": {
                        "format": "%(message)s $(lineno)d $(filename)s",
                        "class": "pythonjsonlogger.jsonlogger.JsonFormatter",
                    }
                },
                "handlers": {
                    "json": {"class": "logging.StreamHandler", "formatter": "json"}
                },
                "loggers": {"": {"handlers": ["json"], "level": level}},
            }
        )
        structlog.configure(
            context_class=structlog.threadlocal.wrap_dict(dict),
            logger_factory=structlog.stdlib.LoggerFactory(),
            wrapper_class=structlog.stdlib.BoundLogger,
            cache_logger_on_first_use=True,
            processors=[
                structlog.stdlib.filter_by_level,
                structlog.stdlib.add_logger_name,
                structlog.stdlib.add_log_level,
                structlog.stdlib.PositionalArgumentsFormatter(),
                structlog.processors.StackInfoRenderer(),
                structlog.processors.format_exc_info,
                structlog.processors.UnicodeDecoder(),
                structlog.stdlib.render_to_log_kwargs,
            ],
        )
    else:
        logging.basicConfig(
            level=level,
            stream=sys.stdout,
            format="[%(asctime)s][%(name)-24s][%(levelname)-8s]: %(message)s",
        )
        structlog.configure(
            logger_factory=structlog.stdlib.LoggerFactory(),
            wrapper_class=structlog.stdlib.BoundLogger,
            cache_logger_on_first_use=True,
            processors=[
                structlog.stdlib.filter_by_level,
                structlog.stdlib.PositionalArgumentsFormatter(),
                structlog.processors.StackInfoRenderer(),
                structlog.processors.format_exc_info,
                structlog.processors.UnicodeDecoder(),
                structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
            ],
        )
    if config.TESTING:
        logging.disable(logging.CRITICAL)


setup_logging()
get_logger = structlog.get_logger
getLogger = structlog.get_logger

LOGGER = get_logger(__name__)


class RequestIdMiddleware:
    def __init__(self, app: ASGIApp) -> None:
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope["type"] not in ["http", "websocket"]:
            await self.app(scope, receive, send)
            return
        if "request_id" not in scope:
            scope["request_id"] = scope.get("apigw-tracking-header", str(uuid4()))
        await self.app(scope, receive, send)


class LoggingMiddleware:
    def __init__(
        self, app: ASGIApp, logger: Any = LOGGER, log_level: int = logging.INFO
    ) -> None:
        self.app = app
        self.logger = logger
        self.level = log_level

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope["type"] not in ["http", "websocket"]:
            await self.app(scope, receive, send)
            return

        method = scope.get("method")
        path = scope.get("path")
        scheme = scope.get("scheme")
        query_string = scope.get("query_string")
        headers = {k.decode("utf-8"): v for k, v in scope.get("headers", [])}
        start_time = perf_counter()

        async def inner_receive():
            message = await receive()
            self.logger.log(
                self.level,
                "request_start",
                method=method,
                path=path,
                headers=headers,
                scheme=scheme,
                query_string=query_string,
                request_id=scope.get("request_id"),
            )
            return message

        async def inner_send(message):
            if message.get("type") == "http.response.start":
                scope["status_code"] = message.get("status")
                scope["response_headers"] = message.get("headers")
                scope["response_headers"] = {
                    k.decode("utf-8"): v for k, v in message.get("headers", [])
                }
            if (
                message.get("type") == "http.response.body"
                and message.get("more_body", False) is False
            ):
                end_time = perf_counter()
                duration = end_time - start_time
                self.logger.log(
                    self.level,
                    "request_end",
                    status_code=scope["status_code"],
                    response_headers=scope.get("response_headers"),
                    duration=duration,
                    request_id=scope.get("request_id"),
                )
            await send(message)

        await self.app(scope, inner_receive, inner_send)
