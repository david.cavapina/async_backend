from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from api.plugins.apispec import StarlettePlugin
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse, Response
from starlette.routing import Mount, Route

from api import config, logging
from api.exceptions import exception_handlers

from api.status.routes import routes as status_routes


log = logging.get_logger(__name__)


async def root(request: Request):
    return JSONResponse({"endpoint": "root"})


CORS_PARAMS_BY_ENVIRONMENT = {
    "production": {"allow_origin_regex": r"https://.*\.backendlessons\.co\.uk"}
}


routes = [
    Route("/", endpoint=root),
    Mount("/status", routes=status_routes),
]

middleware = [
    Middleware(logging.RequestIdMiddleware),
    Middleware(logging.LoggingMiddleware),
    Middleware(
        CORSMiddleware,
        allow_methods=["*"],
        allow_headers=["Authorization"],
        **CORS_PARAMS_BY_ENVIRONMENT.get(config.ENVIRONMENT, {"allow_origins": "*"}),
    ),
]


app = Starlette(
    debug=config.DEBUG,
    routes=routes,
    exception_handlers=exception_handlers,
    middleware=middleware,
)

# docs
plugin = StarlettePlugin(app)
spec = APISpec(
    title="Backend Lessons API",
    version="0.2",
    openapi_version="3.0.2",
    plugins=[MarshmallowPlugin(), plugin],
)


@app.route("/swagger.json", include_in_schema=False)
def schema(request: Request) -> Response:
    for endpoint in plugin.endpoints():
        spec.path(path=endpoint.path, endpoint=endpoint)

    if "X-Forwarded-Prefix" in request.headers:
        spec.options.setdefault("basePath", request.headers["X-Forwarded-Prefix"])

    return JSONResponse(spec.to_dict())


@app.route("/swagger", include_in_schema=False)
async def show_swagger_ui(request):
    context = {"request": request, "url": "/swagger.json"}
    return config.templates.TemplateResponse("swagger.html", context)
