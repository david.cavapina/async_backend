# Relationship between API and Database
from sqlalchemy import Column, Integer, String, text

from api.db import ToDictMixin, AuditMixin, BaseModel


class Status(ToDictMixin, AuditMixin, BaseModel):
    __tablename__ = "status"

    id = Column(Integer, primary_key=True)
    uuid = Column(String, unique=True, server_default=text("uuid_generate_v4()"))
    name = Column(String(), nullable=True)
    description = Column(String(), nullable=True)
