from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.routing import Route

from api import logging

log = logging.get_logger(__name__)


async def ok(request: Request):
    """Return OK
    ---
    description: Return OK
    responses:
        200:
            description: Returns OK
    """
    return JSONResponse({"status": "ok"})


async def error(request: Request):
    """Return an error
    ---
    description: Return an error
    responses:
        500:
            description: Generates an error
    """
    raise Exception()


routes = [
    Route("/ok", ok, methods=["GET"]),
    Route("/error", error, methods=["GET"], include_in_schema=False),
]
