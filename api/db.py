from contextlib import asynccontextmanager

import databases
import sqlalchemy
from marshmallow import fields
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from starlette.middleware.base import BaseHTTPMiddleware

from api import logging
from api.config import DATABASE_URL, TESTING

log = logging.get_logger(__name__)

# Database table definitions.
metadata = sqlalchemy.MetaData()
BaseModel = declarative_base(metadata)
db = databases.Database(DATABASE_URL, max_size=10)


@asynccontextmanager
async def connected():
    if not db.is_connected:
        await connect()
        yield db
        await disconnect()
    else:
        yield db


async def connect():
    log.info(f"Connecting to {DATABASE_URL}")
    await db.connect()


async def disconnect():
    log.info(f"Disconnecting from {DATABASE_URL}")
    await db.disconnect()


class TransactionMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        # TODO: the exception handling logic is triggered _before_ this mw
        # so the transaction is always committed on errors.
        if TESTING:
            return await call_next(request)

        async with db.transaction():
            return await call_next(request)


class ToDictMixin:
    def to_dict(self, include_none=True):
        return {
            column.name: getattr(self, column.name)
            for column in self.__table__.columns
            if getattr(self, column.name) is not None or include_none
        }

    def to_params(self):
        return self.to_dict(include_none=False)


class AuditMixin(object):
    created_at = sqlalchemy.Column(sqlalchemy.DateTime, default=func.now())
    updated_at = sqlalchemy.Column(
        sqlalchemy.DateTime, default=func.now(), onupdate=func.now()
    )


class AuditSchemaMixin(object):
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
    deleted_at = fields.DateTime(required=False, allow_none=True)
