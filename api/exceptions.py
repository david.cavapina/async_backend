from asyncpg.exceptions import PostgresError
from marshmallow.exceptions import ValidationError
from starlette.authentication import AuthenticationError
from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse

from api import logging

log = logging.get_logger(__name__)


class Unauthorized(HTTPException):
    def __init__(self, detail="Unauthorized"):
        super().__init__(401, detail=detail)


class Forbidden(HTTPException):
    def __init__(self, detail="Forbidden"):
        super().__init__(403, detail=detail)


class MethodNotAllowed(HTTPException):
    def __init__(self, detail="Method Not Allowed"):
        super().__init__(405, detail=detail)


class NotFound(HTTPException):
    def __init__(self, detail="Not Found"):
        super().__init__(404, detail=detail)


class BadRequest(HTTPException):
    def __init__(self, detail="Bad Request"):
        super().__init__(400, detail=detail)


class BadData(HTTPException):
    def __init__(self, detail="Bad Data"):
        super().__init__(400, detail=detail)


def create_exception(detail, status_code):
    return JSONResponse(
        {"detail": detail, "status": status_code}, status_code=status_code
    )


async def http_exception(request, exc):
    return create_exception(exc.detail, exc.status_code)


async def unhandled_exception(request, exc):
    return create_exception("Internal Server Error", 500)


async def validation_exception(request, exc):
    return create_exception(str(exc), 400)


async def authentication_exception(request, exc):
    return create_exception(str(exc), 401)


async def database_error(request, exc):
    return create_exception("Bad Request", 400)


exception_handlers = {
    HTTPException: http_exception,
    ValidationError: validation_exception,
    AuthenticationError: authentication_exception,
    PostgresError: database_error,
    Exception: unhandled_exception,
}
