Backend Lessons
====

## Prerequisites

1. [Python interpreter](https://www.python.org/downloads/) `>=3.7.0`
2. [Python virtual environment](https://www.freecodecamp.org/news/manage-multiple-python-versions-and-virtual-environments-venv-pyenv-pyvenv-a29fb00c296f/)
3. [local PostgreSQL instance](https://www.postgresql.org/docs/10/tutorial-start.html) with two databases (`createdb belessons` and `createdb belessons_test`), the default db configuration is defined in `.env` file, change those values if your db configuration differs. In case you need to delete them: (`dropdb belessons` and `dropdb belessons_test`)

## Quick start

1. Checkout the project with `git clone git@gitlab.com:david.cavapina/async_backend.git && cd aync_backend/`
3. Activate the Python virtual environment in which we will install all the project specific dependecies (_the [Prerequisites]( ##Prerequisites) section describes the different options how to create one_)
4. Run `make develop` to install the dev dependencies
5. Run the webserver with `make api`


### Starting the ASGI server

This application is built with [Starlette](https://www.starlette.io/), a lightweight ASGI framework/toolkit. To run it, we need an ASGI server, in this project we are using [uvicorn](https://www.uvicorn.org/).

Run `make api` will start the server on port `8082` for the API.


### Tests

The python library `py.test` is employed for testing. `py.test` implements all aspects of configuration, collection, running and reporting by calling well specified hooks. Virtually any Python module can be registered as a plugin. By specifying `conftest.py` files you can make available the methods/hooks you need for your tests. You can have just one global one, or one per directory per group of tests.

Do not forget to include `__init__.py` files in the directories you want python to consider as modules so the `.py` files in them are found.

1. Run `make test` or `source .venv/bin/pytest tests/<path_to_test>::<test_name>


### Database Management

Alembic provides for the creation, management, and invocation of change management scripts for a relational database, using SQLAlchemy as the underlying engine.

The migration environment is created just once, and is then maintained along with the application’s source code itself. The environment is created using the `alembic init <directory>` command and is then customizable to suit the specific needs of the application.

```
yourproject/
    migrations/
        env.py
        README
        script.py.mako
        versions/
            3512b954651e_add_account.py
            2b1ae634e5cd_add_order_id.py
            3adcc9a56557_rename_username_field.py
```

#### DB Tables -> Python Objects

SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL.

```
class ModelObjectSingular(ToDictMixin, AuditMixin, BaseModel):
    __tablename__ = "model_name_plural"
    id = Column(Integer, primary_key=True)
    uuid = Column(String, unique=True, default=text("uuid_generate_v4()"))
    name = Column(String)
```

#### Generating a migration

Models are defined using SQLAlchemy ORM, however, there is not async support for it,
so queries are performed using [Databases](https://www.encode.io/databases/) which
uses SQLAlchemy Core as a query builder.

When making changes to a model keep in mind that fields default to being `nullable`. When
setting defaults you _MUST_ use
[`server_default`](https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Column.params.server_default),
as opposed to `default` and, if the value is a string, it should _not_ be wrapped
in a `text` function as it will remove the quotes in the end SQL query, i.e.:

```
debug = Column(Boolean, nullable=False, default=text("false"))
```

is correct because `false` is not a string but a SQL expression.

As opposed to:

```
locale = Column(String(), nullable=False, server_default="en-GB")
```

Which is a string and adding `text` will remove the quotes on the resulting SQL
query and fail.

To generate a migration:

1. Make sure the model is imported into `migrations/env.py`
2. Run `alembic revision --autogenerate -m "DESCRIPTION"` to generate the migrations
3. The migration will be added to `migrations/versions`
4. To apply the migration run `alembic upgrade head`

A helpful note: if you ever get confused on order of migrations, use `alembic history` command in the terminal in case you get head conflicts.

#### Troubleshooting conflicting migration files:

1. run `alembic history` to check to which revision the current head is pointing to
2. in case the head is pointing to more than one migration revision (because new migrations were made on different branches and then merged), open the conflicted files and adjust the revision and down revision numbers manually so that it's linear again
3. to undo the last migration run `alembic downgrade -1` or go back to a specific migration revision `alembic downgrade <revision number>`
4. as always try to first pull from master and make sure you have the latest changes before you gerate a new migration to avoid conflicting migration files/revision numbers


## Appendix

- [Starlette Docs](https://www.starlette.io/)
- [Starlette-apispec](https://pypi.org/project/starlette-apispec/)
- [SchemaSpy Docs](https://schemaspy.readthedocs.io/en/latest/overview.html)
- [PyTest Docs](https://docs.pytest.org/en/6.2.x/)
- [Factory Boy](https://pypi.org/project/factory-boy/)
- [Alembic](https://alembic.sqlalchemy.org/en/latest/tutorial.html)
- [SQLAlchemy](https://www.sqlalchemy.org/)