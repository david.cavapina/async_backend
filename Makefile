_update-deps:
	pip-compile --allow-unsafe --build-isolation --generate-hashes --output-file requirements/main.txt requirements/main.in
	pip-compile --allow-unsafe --build-isolation --generate-hashes --output-file requirements/dev.txt requirements/dev.in

_venv:
	@python3 -m venv .venv

_install: _venv
	@source .venv/bin/activate; \
		pip install --upgrade -r requirements/main.txt; \
		pip install --upgrade -r requirements/dev.txt;

_setup: _venv
	@source .venv/bin/activate; \
		pip install --upgrade pip-tools pip setuptools pre-commit;\
		pre-commit install; \

develop: _setup _update-deps _install

add: update-deps _install

test:
	TESTING=true pytest -vv

lint:
	flake8 api tests common
	black --check api tests common

fix:
	black api tests common

migrate:
	alembic upgrade head

api:
	uvicorn --port 8082 --no-access-log main:application --reload


.PHONY: update-deps develop migrate worker api private_api build docs docs_serve
